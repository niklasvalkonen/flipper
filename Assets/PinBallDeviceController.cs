﻿using UnityEngine;
using System.Collections;

public class PinBallDeviceController : MonoBehaviour {

    public Transform leftFlipper;
    public Transform rightFlipper;
    public Transform ballPrefab;
    public Transform spawnPoint;
    
    public float force = 100.0f;

    void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            leftFlipper.rigidbody.AddTorque(Vector3.up * -force);
        }
        if (Input.GetKeyUp(KeyCode.LeftControl))
        {
            leftFlipper.rigidbody.AddTorque(Vector3.up * force);
        }
        if (Input.GetKeyDown(KeyCode.RightControl))
        {
            rightFlipper.rigidbody.AddTorque(Vector3.up * -force);
        }
        if (Input.GetKeyUp(KeyCode.RightControl))
        {
            rightFlipper.rigidbody.AddTorque(Vector3.up * force);
        }

        //if (Input.GetAxis("InsertCoin") == 1.0f)
        if (Input.GetKeyDown(KeyCode.F1))
        {
            Instantiate(ballPrefab, spawnPoint.position, Quaternion.identity);
            
        }
    }


}
